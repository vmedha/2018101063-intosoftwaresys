from flask import Flask, render_template, flash, request, redirect, url_for, session, logging
from flask_sqlalchemy import SQLAlchemy
#from data import Articles
#from flask_mysqldb import MySQL
#from wtforms import Form, StringField, TextAreaField, PasswordField, validators
#from passlib.hash import sha256_crypt

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

@app.route('/')
def index():
    return render_template('introduction.html')

@app.route('/introduction.html')
def introduction():
    return render_template('introduction.html')
    
@app.route('/theory.html')
def theory():
    return render_template('theory.html')

@app.route('/objective.html')
def objective():
    return render_template('objective.html')

@app.route('/experiment.html')
def experiment():
    return render_template('experiment.html')

@app.route('/quizzes.html')
def quizzes():
    return render_template('quizzes.html')

@app.route('/procedure.html')
def procedure():
    return render_template('procedure.html')

@app.route('/further_readings.html')
def further_readings():
    return render_template('further_readings.html')

@app.route('/feedback.html')
def feedback():
    return render_template('feedback.html')

class newEntry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    input = db.Column(db.Integer)
    def __init__(self, input):
        self.input = input

@app.route('/submit', methods=['POST'])
def submit():
    inputdata = request.form['inputbox']
    if int(inputdata) < 1 or int(inputdata) > 4:
        return render_template('quizzes.html', errorvalid=1, errormessage='Input valid option!')
    else:
        db.create_all()
        newData = newEntry(inputdata)
        db.session.add(newData)
        db.session.commit()
        return render_template('quizzes.html', errorvalid=0)

if __name__ == '__main__':
    app.run(debug=True)
