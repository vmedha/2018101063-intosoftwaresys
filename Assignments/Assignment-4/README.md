# Context Free Grammar : Computational Lingustics Lab 

### Project By : Medha Vempati (2018101063) & Santhosh Reddy Mylaram (2018101030)

**Follow these steps to run the app :**
1) Install Flask :
        
        sudo apt-get install python3-dev python3-pip
        
        pip3 install flask
        
2) Run :
        ```
        python3 app.py
        ```
3) Open link on localhost :  http://127.0.0.1:5000/

**About The App :**

--> Pages : Introduction, Theory, Objective, Experiment, Quizzes, Procedure, Further Readings, Feedback

--> A brief intro to the idea of context free grammar can be found on the **introduction** page, while a deeper understanding of what it is exactly is available on the **theory** page

--> The objective of the experiment is specified on the **objective** page

--> Detailed explanation of how to perform the experiment can be found in the **procedure** page

--> The experiment can be done on the **experiment** page

--> To test your understanding of Context Free Grammar, head over to the **quizzes** page

--> To increase your knowledge on Context Free Grammar, references are given on the **further readings** page

**Details On Experiment Procedure :**
1) After selecting the sentence, select any two words at a time, along with a chunk name.
2) Choose the same chunk name for the each step in forming the tree.

**Project Structure :**

**app.py** --> This is the main python app file which connects all the other files.

**templates** --> Contains all HTML files to which the main app.py file routes.

**static** --> Contains the CSS and JS files, images, and other dependencies



