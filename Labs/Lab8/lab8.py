class matrix(object):
    

    def __init__(self, rows, cols, Mat):
        if(matrix == None):
            self.mat = [[0 for i in range(cols)] for j in range(rows)]
            self.rows = rows
            self.cols = cols
        else :
            self.mat = [[Mat.mat[i][j] for i in range(cols)] for j in range(rows)]
            
    def __str__(self):
        print ('something')

    def transpose(self):
        temp = matrix(self.cols, self.rows, None)

        for i in range(self.cols) :
            for j in range(self.rows):
                temp.mat[i][j] = self.mat[j][i]

        return temp

    def __setitem__(self, ind, value):
        self.mat[ind[0]][ind[1]] = value
        
    def __getitem__(self, ind):
        return self.mat[ind[0]][ind[1]]

    def __add__(self, matrix2):
        if(self.rows != matrix2.rows or self.cols != matrix2.cols):
            print('Invalid addition operation\n')
            return
        res = matrix(self.rows, self.cols, None)
        for i in range(self.rows):
            for j in range(self.cols):
                res.mat[i][j] = self.mat[i][j] + matrix2.mat[i][j]
        
        return res

    def __sub__(self, matrix2):
        if(self.rows != matrix2.rows or self.cols != matrix2.cols):
            print('Invalid subtraction operation\n')
            return
        res = matrix(self.rows, self.cols, None)
        for i in range(self.rows):
            for j in range(self.cols):
                res.mat[i][j] = self.mat[i][j] - matrix2.mat[i][j]
        
        return res

    def __pow__(self, matrix2):
        if(self.cols != matrix2.rows or self.cols != matrix2.cols):
            print('Invalid multiplication operation\n')
            return
        res = matrix(self.rows, self.cols, None)
        for i in range(self.rows):
            for j in range(self.cols):
                res.mat[i][j] = self.mat[i][j] * matrix2.mat[i][j]
        
        return res

    def __floordiv__(self, matrix2):
        if(self.rows != matrix2.rows or self.cols != matrix2.cols):
            print('Invalid division operation\n')
            return
        res = matrix(self.rows, self.cols, None)
        for i in range(self.rows):
            for j in range(self.cols):
                res.mat[i][j] = self.mat[i][j] / matrix2.mat[i][j]
        
        return res
        
    def __eq__(self, matrix2):
        if(self.rows != matrix2.rows or self.cols != matrix2.cols):
            print('Invalid comparison\n')
            return
        res = matrix(self.rows, self.cols, None)
        for i in range(self.rows):
            for j in range(self.cols):
                if(self.mat[i][j] == matrix2.mat[i][j]):
                    res[i][j] = True
                else:
                    res[i][j] = False
        return res

    def __ne__(self, matrix2):
        if(self.rows != matrix2.rows or self.cols != matrix2.cols):
            print('Invalid comparison\n')
            return
        res = matrix(self.rows, self.cols, None)
        for i in range(self.rows):
            for j in range(self.cols):
                if(self.mat[i][j] != matrix2.mat[i][j]):
                    res[i][j] = True
                else:
                    res[i][j] = False
        return res

    def __gt__(self, matrix2):
        if(self.rows != matrix2.rows or self.cols != matrix2.cols):
            print('Invalid comparison\n')
            return
        res = matrix(self.rows, self.cols, None)
        for i in range(self.rows):
            for j in range(self.cols):
                if(self.mat[i][j] > matrix2.mat[i][j]):
                    res[i][j] = True
                else:
                    res[i][j] = False
        return res

    def __ge__(self, matrix2):
        if(self.rows != matrix2.rows or self.cols != matrix2.cols):
            print('Invalid comparison\n')
            return
        res = matrix(self.rows, self.cols, None)
        for i in range(self.rows):
            for j in range(self.cols):
                if(self.mat[i][j] >= matrix2.mat[i][j]):
                    res[i][j] = True
                else:
                    res[i][j] = False
        return res

    def __le__(self, matrix2):
        if(self.rows != matrix2.rows or self.cols != matrix2.cols):
            print('Invalid comparison\n')
            return
        res = matrix(self.rows, self.cols, None)
        for i in range(self.rows):
            for j in range(self.cols):
                if(self.mat[i][j] <= matrix2.mat[i][j]):
                    res[i][j] = True
                else:
                    res[i][j] = False
        return res

    def __lt__(self, matrix2):
        if(self.rows != matrix2.rows or self.cols != matrix2.cols):
            print('Invalid comparison\n')
            return
        res = matrix(self.rows, self.cols, None)
        for i in range(self.rows):
            for j in range(self.cols):
                if(self.mat[i][j] < matrix2.mat[i][j]):
                    res[i][j] = True
                else:
                    res[i][j] = False
        return res

class vector(matrix):
    
    #def __setitem__ (self, ind, value):

def chooseOp():
    print('1.Create Matrix\n2.Transpose of matrix\n3.Operate on matrix\n4.Compare two matrices\n')
    op = (int)input('Enter operation')
    operation(op)

def operation(argument):
    switcher = {
        
    }

if __name__ == "__main__" :
    chooseOp()