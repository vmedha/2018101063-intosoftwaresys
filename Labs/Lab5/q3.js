var marks = require("./data.js");

function getSum(marks[person]){

	var sum=0;

	for(int i=0; i<5; i++){
		sum += marks[person][i];	
	}
	return sum;
}

function getHighestMarks(marks){

	var maxMarks=0;
	var topName = null;

	for(person in marks){
		var tempSum = getSum(marks['person']);
		if(tempSum > maxMarks){
			maxMarks = tempSum;
			topName = person;
		}
	}

	console.log(topName+' has the highest marks');
}

function getSub2Toppers(marks){
	var maxMarks = 0;
	var names = [];
	var tempArr = [];

	for(person in marks){
		names.push(person);	
	}

	names.sort(function(a,b){names[b][1]-names[a][1]});

	console.log('Decending order of sub 2 marks belong to '+names);

}

//getHighestMarks(marks);
//getSub2Toppers(marks);
