function myArray(array){
  console.log('1)Sum\n2)Product\n3)Sort\n4)Modify\n5)Display\n');
  var operation = prompt('Enter number for corresponding operation');

  switch(operation){
    case 1:
      Sum(array);
      break;
    case 2:
      Product(array);
      break;
    case 3:
      Sort(array);
      break;
    case 4:
      Modify(array);
      break;
    case 5:
      Display(array);
      break;
    default:
      console.log('Enter valid number');
  }
}

function Sum(array){
  //SUM
    console.log('Sum:');
    var sum=0;
    for(i in array){
      sum += i;
    }
    console.log(sum);
}

function Product(array){
  //PRODUCT
    console.log('Product:');
    var prod=1;
    for(i in array){
      prod *= i;
    }
    console.log(prod);
}

function Sort(array){
  //SORT
    console.log('Sort:');
    tempArr = array;
    console.log(tempArr.sort());
}

function Modify(array){
  //modify
    var index = prompt('Please enter index to be modified');
    var new_el = prompt('Please enter new value');
    console.log('Modify: At index '+index);
    array[index] = new_el;
    console.log('Modified Array:'+array);

}

function Display(array){
  console.log(array);
}

var array = [4, 1, 7, 2, 9, 3, 5, 8, 6];
myArray(array);
